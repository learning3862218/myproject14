﻿#include <iostream>
#include <string>

int main()
{
    std::string Test{ "1234abcd" };
    if (!Test.empty()) 
    {
        int Length = Test.length();
        std::cout << "String: " << Test << std::endl << "Length: " << Length << std::endl <<
            "First symbol: " << Test[0] << std::endl << "Last symbol: " << Test[Length - 1] << std::endl;
    }
}
